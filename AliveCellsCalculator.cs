namespace GameOfLife
{
    internal class AliveCellsCalculator
    {
        private readonly CellGrid _cellGrid;

        public AliveCellsCalculator(CellGrid cellGrid)
        {
            _cellGrid = cellGrid;
        }
        
        public int GetSurroundingAliveCells(Position position)
        {
            return GetTopRowAliveCells(position) +
                   GetCurrentRowAliveCells(position) +
                   GetBottomRowAliveCells(position);
        }

        private int GetTopRowAliveCells(Position position)
        {
            return _cellGrid.GetCellAtPosition(new Position(position.Top,position.Left)) + 
                   _cellGrid.GetCellAtPosition(new Position(position.Top,position.X)) + 
                   _cellGrid.GetCellAtPosition(new Position(position.Top,position.Right));
        }

        private int GetCurrentRowAliveCells(Position position)
        {
            return _cellGrid.GetCellAtPosition(new Position(position.Y, position.Left)) + 
                   _cellGrid.GetCellAtPosition(new Position(position.Y, position.Right));
        }

        private int GetBottomRowAliveCells(Position position)
        {
            return _cellGrid.GetCellAtPosition(new Position(position.Bottom,position.Left)) + 
                   _cellGrid.GetCellAtPosition(new Position(position.Bottom,position.X)) + 
                   _cellGrid.GetCellAtPosition(new Position(position.Bottom,position.Right));
        }
    }
}