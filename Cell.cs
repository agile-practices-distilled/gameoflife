namespace GameOfLife
{
    internal class Cell
    {
        private int  _status;

        private const int ALIVE = 1;
        private const int DEAD = 0;
        
        public Cell(int cellStatus)
        {
            _status = cellStatus;
        }

        public bool IsAlive()
        {
            return _status == ALIVE;
        }

        public void Kill()
        {
            _status = DEAD;
        }

        public void Revive()
        {
            _status = ALIVE;
        }

        public Cell Clone()
        {
            return new Cell(_status);
        }
        
        public static int operator+ (Cell a, Cell b) {
            
            return a._status + b._status;
        }
        
        public static int operator+ (int a, Cell b) {
            
            return a + b._status;
        }

        public override string ToString()
        {
            return _status  == 0 ? "D" : "A";
        }
    }
}