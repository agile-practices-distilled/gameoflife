namespace GameOfLife
{
    class Position
    {
        public readonly int X;
        public readonly int Y;

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int Top => Y + 1;
        public int Bottom => Y - 1;
        public int Left => X - 1;
        public int Right => X + 1;
    }
}