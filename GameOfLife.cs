namespace GameOfLife
{
    public class GameOfLife
    {
        private CellGrid _cellGrid;
        private CellGrid _cellGridCopy;
        private readonly int _maxSurroundedCellIndex;

        public GameOfLife(int[,] cellGrid)
        {
            _cellGrid = new CellGrid(Initialize(cellGrid));
            _maxSurroundedCellIndex = cellGrid.GetLength(0) - 1;
        }

        public void Tick()
        {
            _cellGridCopy = _cellGrid.Clone();
            
            for (int rowIndex = 1; rowIndex < _maxSurroundedCellIndex; rowIndex++)
            {
                RowTick(rowIndex);
            }

            _cellGrid = _cellGridCopy.Clone();
        }

        public override string ToString()
        {
            return _cellGrid.ToString();
        }

        private void RowTick(int rowIndex)
        {
            for (int columnIndex = 1; columnIndex < _maxSurroundedCellIndex; columnIndex++)
            {
                UpdateCells(new Position(rowIndex, columnIndex));
            }
        }

        private void UpdateCells(Position cellPosition)
        {
            var aliveCells = new AliveCellsCalculator(_cellGrid)
                .GetSurroundingAliveCells(cellPosition);

            if (_cellGrid.IsAlive(cellPosition) && (aliveCells < 2 || aliveCells > 3))
                _cellGridCopy.KillCell(cellPosition);

            if (!_cellGrid.IsAlive(cellPosition) && aliveCells == 3)
                _cellGridCopy.ReviveCell(cellPosition);
        }

        private static Cell[,] Initialize(int[,] cellGrid)
        {
            var gridSize = cellGrid.GetLength(0);
            
            var result = new Cell[gridSize,gridSize];
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    result[i,j] = new Cell(cellGrid[i,j]);
                }
            }

            return result;
        }
    }
}