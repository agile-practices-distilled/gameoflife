using System.Text;

namespace GameOfLife
{
    internal class CellGrid
    {
        private readonly Cell[,] _cellGrid;
        private readonly int _gridSize;
        
        public CellGrid(Cell[,] cellGrid)
        {
            _gridSize = cellGrid.GetLength(0);
            _cellGrid = cellGrid;
        }

        public bool IsAlive(Position position)
        {
            return _cellGrid[position.X, position.Y].IsAlive();
        }

        public void KillCell(Position position)
        {
            _cellGrid[position.X, position.Y].Kill();
        }

        public void ReviveCell(Position position)
        {
            _cellGrid[position.X, position.Y].Revive();
        }

        public Cell GetCellAtPosition(Position position)
        {
            return _cellGrid[position.X, position.Y];
        }

        public CellGrid Clone()
        {
            var result = new Cell[_gridSize,_gridSize];
            for (int i = 0; i < _gridSize; i++)
            {
                for (int j = 0; j < _gridSize; j++)
                {
                    result[i, j] = _cellGrid[i, j].Clone();
                }
            }

            return new CellGrid(result);
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            for (int rowIndex = 0; rowIndex < _gridSize; rowIndex++)
            {
                result.AppendLine(RowToString(rowIndex));
            }

            return result.ToString();
        }

        private string RowToString(int rowIndex)
        {
            var rowCellValues = new string[_gridSize];
            for (int columnIndex = 0; columnIndex < _gridSize; columnIndex++)
            {
                rowCellValues[columnIndex] = _cellGrid[rowIndex, columnIndex].ToString();
            }

            return string.Join('|', rowCellValues);
        }
    }
}