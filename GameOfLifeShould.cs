﻿using NUnit.Framework;

namespace GameOfLife
{
    public class GameOfLifeShould
    {
        [Test]
        public void kill_living_cells_with_fewer_than_2_alive_neighbors()
        {
            var grid = new[,]
            {
                {0, 1, 0},
                {0, 1, 0},
                {0, 0, 0}
            };
            var gameOfLife = new GameOfLife(grid);

            gameOfLife.Tick();

            Assert.AreEqual("" +
                            "D|A|D\n" +
                            "D|D|D\n" +
                            "D|D|D\n", gameOfLife.ToString());
        }

        [Test]
        public void survive_living_cells_with_2_or_3_alive_neighbors()
        {
            var grid = new[,]
            {
                {0, 1, 1},
                {0, 1, 0},
                {0, 0, 0}
            };
            var gameOfLife = new GameOfLife(grid);

            gameOfLife.Tick();

            Assert.AreEqual("" +
                            "D|A|A\n" +
                            "D|A|D\n" +
                            "D|D|D\n", gameOfLife.ToString());
        }

        [Test]
        public void kill_living_cells_with_more_than_3_alive_neighbors()
        {
            var grid = new[,]
            {
                {0, 1, 1},
                {0, 1, 0},
                {1, 1, 0}
            };
            var gameOfLife = new GameOfLife(grid);

            gameOfLife.Tick();

            Assert.AreEqual("" +
                            "D|A|A\n" +
                            "D|D|D\n" +
                            "A|A|D\n", gameOfLife.ToString());
        }

        [Test]
        public void convert_dead_cells_into_living_cells_when_exactly_3_alive_neighbors()
        {
            var grid = new[,]
            {
                {0, 1, 1},
                {0, 0, 0},
                {0, 1, 0}
            };
            var gameOfLife = new GameOfLife(grid);

            gameOfLife.Tick();

            Assert.AreEqual("" +
                            "D|A|A\n" +
                            "D|A|D\n" +
                            "D|A|D\n", gameOfLife.ToString());
        }
        
        [Test]
        public void apply_rules_simultaneously()
        {
            var grid = new[,]
            {
                {0, 0, 0, 0},
                {0, 1, 1, 1},
                {0, 0, 0, 0},
                {0, 0, 1, 0},
            };
            var gameOfLife = new GameOfLife(grid);

            gameOfLife.Tick();

            Assert.AreEqual("" +
                            "D|D|D|D\n" +
                            "D|D|A|A\n" +
                            "D|D|D|D\n" +
                            "D|D|A|D\n", gameOfLife.ToString());
        }
    }
}